
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

        static int wait_size;
        static int sock;
        static  struct sockaddr_in socket_address;
        int fd;
         struct sockaddr_in clientname;
        static size_t connection_size;
        int bytesRead = 0;
        int bytesWritten = 0;

        
        #define MSG_MAXBUFFERSIZE 1096

        static char input_buffer[MSG_MAXBUFFERSIZE];
        static char output_buffer[MSG_MAXBUFFERSIZE];

        size_t connSize;

        int readClient();


        
void main()
{
      memset(&wait_size, 0, sizeof(int));
        wait_size = 32;

        memset(&sock, 0, sizeof(int));

 
    sock = socket (AF_INET, SOCK_STREAM, 0);



    if (sock < 0)
    {
        printf("SERVER: SOCKET ERROR\n");
        exit(-2001);
    }else{
        printf("socket created successfully \n");
    }

    memset(&socket_address, 0, sizeof(struct sockaddr_in));
    socket_address.sin_family = AF_INET;
    socket_address.sin_port = htons (9997);
    // socket_address.sin_addr.s_addr = inet_addr("127.0.0.1");
    socket_address.sin_addr.s_addr = inet_addr("192.168.88.133");
    

    if(bind (sock, (struct sockaddr *) &socket_address, sizeof (socket_address)) < 0)
    {
        printf("SERVER: BIND ERROR\n");
        exit(-2002);
    }else{
        printf("bind success \n");
    }

    if(listen (sock,wait_size) < 0)
    {
        printf("SERVER: LISTEN: Error \n");
        exit(-2004);
    }else{
        printf("sockeet listening for connections \n");
    }


    connSize  = sizeof (clientname);
    


    while (1)
    {


        fd = accept(sock,(struct sockaddr *) &clientname, &connSize);
         // fd = accept(sock,NULL, NULL);

        if (fd < 0)
        {
            printf("SERVER: ACCEPT ERROR %d \n", errno);
            exit(-2003);
        }else{
            printf("accept success \n");
        }

        while(readClient() == 0);



    }
}



int readClient()
{
        memset(&output_buffer[0], 0, MSG_MAXBUFFERSIZE);
        memset(&input_buffer[0], 0, MSG_MAXBUFFERSIZE);

        printf("attempting to Read data from client \n");
        bytesRead = read (fd, &input_buffer[0], MSG_MAXBUFFERSIZE);
        printf("Read finished \n");

        if (bytesRead < 0)
        {
          printf("SERVER: READ ERROR\n");
          //printf(,"\n bytes_read less than 0\n");
          exit(-2005);
        }
        else if (bytesRead == 0)
        {
            printf("\n bytes_read wqaul to  0\n");
          return -1;
        }
        else
        {
            printf("bytes read %d \n", bytesRead);
            /* opcua ACK message */
            output_buffer[0] = 0x41;
            output_buffer[1] = 0x43;
            output_buffer[2] = 0x4b;
            output_buffer[3] = 0x46;
            output_buffer[4] = 0x1c;
            output_buffer[5] = 0x00;
            output_buffer[6] = 0x00;
            output_buffer[7] = 0x00;
            output_buffer[8] = 0x00;
            output_buffer[9] = 0x00;
            output_buffer[10] = 0x00;
            output_buffer[11] = 0x00;
            output_buffer[12] = 0xff;
            output_buffer[13] = 0xff;
            output_buffer[14] = 0x00;
            output_buffer[15] = 0x00;
            output_buffer[16] = 0xff;
            output_buffer[17] = 0xff;
            output_buffer[18] = 0x00;
            output_buffer[19] = 0x00;
            output_buffer[20] = 0x00;
            output_buffer[21] = 0x00;
            output_buffer[22] = 0x00;
            output_buffer[23] = 0x00;
            output_buffer[24] = 0x00;
            output_buffer[25] = 0x00;
            output_buffer[26] = 0x00;
            output_buffer[27] = 0x00;

            
            bytesWritten = write(fd, &output_buffer[0], 28);
            printf("witten %d bytes \n", bytesWritten);
        }

        return 0;
}

      
